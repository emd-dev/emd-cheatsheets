\documentclass[usenames,dvipsnames]{beamer}
\usepackage{tikz}
\usepackage[orientation=landscape,size=custom,width=64,height=32]{beamerposter}
\usepackage[11pt]{moresize}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{booktabs}

\usepackage{xcolor}
\definecolor{smokewhite}{HTML}{F8F8F8}
\definecolor{slate}{HTML}{2F4F4F}
\definecolor{steel}{HTML}{4682B4}
\definecolor{teal}{HTML}{80B080}
\definecolor{goldenrod}{HTML}{B8860B}

\definecolor{set2_1}{HTML}{66c2a5}
\definecolor{set2_2}{HTML}{fc8d62}
\definecolor{set2_3}{HTML}{8da0cb}
\definecolor{set2_4}{HTML}{e78ac3}
\definecolor{set2_5}{HTML}{a6d854}

\usepackage{libertine}
\setlength{\paperwidth}{64in}
\setlength{\paperheight}{32in}

%\usepackage{listings}


\usepackage[none]{hyphenat}
\usetikzlibrary{backgrounds}
\usetikzlibrary{positioning}
\linespread{1.1}


% Default fixed font does not support bold face
\usepackage[T1]{fontenc}


% Custom colors
\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}

\usepackage{minted}
\usepackage{listings}

\lstset{aboveskip=\baselineskip,belowskip=\baselineskip,basicstyle=\ttfamily,backgroundcolor = \color{smokewhite},escapeinside={\#},}

\newcommand{\codeSep}{8pt}

%-------------------------------------------------


\begin{document}

\setbeamercolor{background canvas}{bg=white}

\begin{frame}[fragile]

\begin{tikzpicture}[remember picture, overlay]


%\draw (1,32) -- (1,-32);
%\draw (40,32) -- (40,-32);
%\draw (79,32) -- (79,-32);
%\draw (120,32) -- (120,-32);


%-------------------------------------------------
% HEADER


\node[anchor=north west]
  at (1,30) (title)
  {\parbox[t]{38cm}{\centering\usebeamerfont{subtitle}{\bf\fontsize{90}{60}\selectfont EMD v0.4.0}\\ \vspace{1cm}{\bf\fontsize{45}{30}\selectfont Tutorial Cheat Sheet}}};


\node[anchor=north, below =5cm of title]
 (links)
  {\parbox[t]{38cm}{\centering\usebeamerfont{subtitle}{  
  
  Quinn et al., (2021). EMD: Empirical Mode Decomposition and Hilbert-Huang Spectral Analyses in Python. Journal of Open Source Software, 6(59), 2977, {\color{blue} \underline{https://doi.org/10.21105/joss.02977}}
  \vspace{1cm}
  
Documentation : \hyperref[emd.readthedocs.io]{\color{blue} \underline{emd.readthedocs.io}}

Tutorials : \hyperref[https://emd.readthedocs.io/en/stable/emd_tutorials/index.html]{\color{blue} \underline{emd.readthedocs.io/en/stable/emd\_tutorials}}

Development : \hyperref[https://gitlab.com/emd-dev/emd]{\color{blue} \underline{gitlab.com/emd-dev/emd}}

Issue Tracking : \hyperref[https://gitlab.com/emd-dev/emd/-/issues]{\color{blue} \underline{gitlab.com/emd-dev/emd/-/issues}}


   }}};

%--------------------------------------------------
% INSTALL


\node[anchor=north west, fill=white!50, draw=set2_5!35, line width=5mm, 
	minimum height=25cm,
	minimum width=38cm, rounded corners=1cm]
  at (1,5) (install_box) {
\begin{minipage}{36cm}
The latest stable version of the EMD package can be installed for Python 3.5+ using pip.
        
\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{bash}
    $ pip install emd
\end{minted}

A specific version can be installed by specifying the version number after an $==$ sign.
\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{bash}
    $ pip install emd==0.4.0
\end{minted}

The EMD package (and optional version) can also be added to conda envs in the pip subsection

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{bash}
    name: example_emd_env
    dependencies:
       - pip
       - pip:
         - emd==0.4.0
\end{minted}

Once installed, you can import EMD and check that the expected version is installed.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    import emd
    emd.support.get_installed_version()
\end{minted}

\end{minipage}
};

\node[anchor=center,fill=set2_5,draw=set2_5,rounded corners=0.5cm,minimum width=20cm]
  at (install_box.north) (install_title) {\Huge Install};

%--------------------------------------------------
% SIMULATE


\node[anchor=north west, fill=white!50, draw=set2_1!35, line width=5mm, 
	minimum height=25cm,
	minimum width=38cm, rounded corners=1cm]
  at (1,-22) (ref_box) {
\begin{minipage}{36cm}
Simulated time-series with dynamic oscillations can be generated with an autoregressive filter and some additive white noise.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    peak_freq = 12 
    sample_rate = 512
    seconds = 60
    x = emd.utils.ar_simulate(peak_freq, sample_rate, seconds, 
                              noise_std=0.2, random_seed=42,
                              r=.99)
    t = np.linspace(0, seconds, seconds*sample_rate)
\end{minted}

A quick visualisation shows a clear 12Hz oscillation embedded in noise. The oscillation has dynamics in both it's amplitude and in its frequency.

\includegraphics[]{figures/emd_software_fig1.png}


\end{minipage}
};

\node[anchor=center,fill=set2_1,draw=set2_1,rounded corners=0.5cm,minimum width=20cm]
  at (ref_box.north) (ref_title) {\Huge Data Simulation};




%--------------------------------------------------
% THE SIFT


\node[anchor=north west, fill=white!50, draw=set2_2!35, line width=5mm, 
	minimum height=78cm,
	minimum width=38cm, rounded corners=1cm]
  at (41,31) (ref_box) {
\begin{minipage}{36cm}
The classic sift and a range of variants are implemented in the sift module of the EMD package.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    # The classic sift algorithm
    imf = emd.sift.sift(X)
    # The EEMD sift algorithm
    imf = emd.sift.ensemble_sift(X)
    # The CEEMD sift algorithm
    imf = emd.sift.complete_ensemble_sift(X)
    # The masked sift algorithm
    imf = emd.sift.mask_sift(X)    
\end{minted}

The output \mintinline{python}{imf} contains the Intrinsic Mode Functions identified by the sift. It is a numpy array of dimensions [nsamples x nimfs] and can be quickly visualised using the \mintinline{python}{plot_imfs} function.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    emd.plotting.plot_imfs(imf, cmap=True, scale_y=True)
\end{minted}

\begin{center}
\includegraphics[]{figures/emd_software_fig2.png}
\end{center}

Each of the sift functions is highly customisible with a range of different options that might be tuned. These are detailed in the function docstrings and API section of the website. Most can be specified in the function call as keyword arguments.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    imf = emd.sift.ensemble_sift(X, nensembles=24, 
                                 max_imfs=5, nprocesses=4)
\end{minted}

There are three sets of low-level options which can be tuned by specifying dictionaries of arguments. These relate to specific sections of components of the underlying sift algorithm.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    # imf_opts relates to the estimation of a single IMF
    imf_opts = {'stop_method': 'rilling', 'env_step_size': 0.3}
    # envelope_opts relates to envelope interpolation
    envelope_opts = {'interp_method': 'spl_rep'}
    # extrema_opts relates to maxima/minima detection and padding
    extrema_opts = {'pad_width': 4, 'parabolic_extrema': True}
    
    imf = emd.sift.sift(x, imf_opts=imf_opts,
                        envelope_opts=envelope_opts,
                        extrema_opts=extrema_opts)
\end{minted}

These options can get complex, so a \mintinline{python}{SiftConfig} class is available to help manage finely tuned sifts. This is a dictionary containing every tweakable option for a given sift function. These options can be viewed, modified and passed into the corresponding sift function.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    config = emd.sift.get_config('mask_sift')
    print(config)  # view all options
    config['mask_freqs'] = 0.3
    config['nphases'] = 12
    config['envelope_opts/interp_method'] = 'pchip_mono'
    imf = emd.sift.mask_sift(x, **config)
\end{minted}

Once set, these configurations can be saved to a yaml-format text file for easy reuse and sharing.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    # Save a config to disk
    config.to_yaml_file('my_mask_sift.yml')
    # Load a previous config
    config = emd.sift.SiftConfig.from_yaml_file('prev_sift.yml')

\end{minted}


\end{minipage}

};

\node[anchor=center,fill=set2_2,draw=set2_2,rounded corners=0.5cm,minimum width=20cm]
  at (ref_box.north) (ref_title) {\Huge The Sift};


%--------------------------------------------------
% FREQUENCY  & SPECTRA

\node[anchor=north west, fill=white!50, draw=set2_3!35, line width=5mm, 
	minimum height=78cm,
	minimum width=38cm, rounded corners=1cm]
  at (81,31) (ref_box) {
\begin{minipage}{36cm}
Once a set of IMFs have been identified, we use the Hilbert transform to estimate the instantaneous phase, frequency and amplitude for each component.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    # Compute frequency transform
    IP, IF, IA = emd.spectra.frequency_transform(imf, 
                                                 sample_rate, 
                                                 'hilbert')
\end{minted}

The three outputs \mintinline{python}{IP, IF, IA} are the instantaneous phase, instantaneous frequency and instantaneous amplitude respectively. They are numpy arrays of the same size as the input \mintinline{python}{imf}.

\begin{center}
\includegraphics[]{figures/emd_software_fig3.png}
\end{center}

A range of power spectra can be computed from the distribution of instantaneous amplitude across time and instantaneous frequency. This is known as the Hilbert-Huang Transform or HHT.

We first compute the HHT across the whole time-dimension for each IMF.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    # Define a set of bins between 1 and 50Hz in 49 steps
    edges, centres = emd.spectra.define_hist_bins(1, 50, 49)
    # Compute the HHT
    spec = emd.spectra.hilberthuang_1d(IF, IA, edges)
\end{minted}

\begin{center}
\includegraphics[]{figures/emd_software_fig4.png}
\end{center}

The HHT correctly identifies a 12Hz peak in power from our simulated system, with very little power outside IMF-3. We can also compute a 2D HHT which preserves the temporal information in the signal.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    # Compute the HHT
    hht = emd.spectra.hilberthuang(IF, IA, edges)
\end{minted}

\begin{center}
\includegraphics[]{figures/emd_software_fig5.png}
\end{center}

The 2D HHT is summed across all IMFs and provides a high-resolution picture of the time-frequency dynamics in our signal.
A range of extra power spectrum options are implemented in the EMD package, for example \mintinline{python}{emd.spectra.holospectrum} implements 3-dimensional holospectrum analyses that explicitly quantifies amplitude modulations.

\end{minipage}

};

\node[anchor=center,fill=set2_3,draw=set2_3,rounded corners=0.5cm,minimum width=20cm]
  at (ref_box.north) (ref_title) {\Huge Frequency \& Spectra};




%--------------------------------------------------
% CYCLE ANALYSIS


\node[anchor=north west, fill=white!50, draw=set2_4!35, line width=5mm, 
	minimum height=78cm,
	minimum width=38cm, rounded corners=1cm]
  at (121,31) (ref_box) {
\begin{minipage}{36cm}
The EMD package contains a range of tools for analysing single-cycle properties of oscillations identified in IMFs. This functionality is based around the \mintinline{python}{Cycles} class. This is initialised with the instantaneous phase time-course from a single IMF. It identifies all individual cycles and provides functions to help computing statistics and analyses across them.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    C = emd.cycles.Cycles(IP[:,2])
\end{minted}

The indices of individual cycles can be identified using the \mintinline{python}{C.get_inds_of_cycle} method.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    inds = C.get_inds_of_cycle(5)  # Find cycle 5
    cycle_imf = x[inds]  # cycle in IMF (coloured lines)
    cycle_raw = x[inds]  # cycle in raw data (grey lines)
\end{minted}

 \begin{center}
\includegraphics[]{figures/emd_software_fig7.png}
\end{center}

Several helper functions are provided to loop through these individual cycles and apply a function to each in turn. For example, here we compute the maximum IA of each cycle by applying the \mintinline{python}{np.max} function from numpy to the IA time-course of each cycle.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    C.compute_cycle_metric('max_amp', IA[:,2], np.max)
\end{minted}

here we compute the duration of each cycle with  \mintinline{python}{len} and its average IF with  \mintinline{python}{np.mean}.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    C.compute_cycle_metric('duration', IA[:,2], len)
    C.compute_cycle_metric('avg_if', IF[:,2], np.mean)
\end{minted}

and finally, we define a custom metric and evaluate it for each cycle.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    def degree_nonlinearity(x):
          y = np.sum(((x-x.mean()) / x.mean())**2)
          return np.sqrt(y / len(x))

    C.compute_cycle_metric('DoN', IF[:, 2], degree_nonlinearity)
\end{minted}

These metrics can be exported as a pandas dataframe for further analyses

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    df = C.get_metric_dataframe()
\end{minted}

\begin{center}
\small
\begin{tabular}{lrrrrr}
\toprule
index &  is\_good &       max\_amp &  duration &     avg\_if &       DoN \\
\midrule
0   &        0 &   8659.437400 &        24 &   9.399259 &    0.246484 \\
1   &        1 &  10381.756506 &        42 &  12.251503 &    0.057094 \\
2   &        1 &  14046.640341 &        43 &  11.982875 &    0.016793 \\
3   &        1 &  16413.142189 &        44 &  11.658568 &    0.008612 \\
4   &        1 &  16817.458784 &        43 &  11.759604 &    0.009969 \\
$\vdots$  & $\vdots$ & $\vdots$ &$\vdots$  &$\vdots$ & $\vdots$ \\
707 &        1 &  17545.702990 &        44 &  11.447015 &    0.008980 \\
708 &        0 &  10228.968471 &        53 &  14.016061 &    1.447171 \\
\bottomrule
\end{tabular}
\end{center}

A subset of cycles can be defined with a set of conditions based on the defined cycle metrics.

\begin{minted}[frame=lines,framesep=\codeSep,framerule=0pt,bgcolor=smokewhite]{python}
    C.pick_cycle_subset(['duration>30', 'max_amp>12000'])
    df = C.get_metric_dataframe(subset=True)
\end{minted}

The properties and relationships between these metrics can then be analysed.

\begin{center}
\includegraphics[]{figures/emd_software_fig6.png}
\end{center}



\end{minipage}

};

\node[anchor=center,fill=set2_4,draw=set2_4,rounded corners=0.5cm,minimum width=20cm]
  at (ref_box.north) (ref_title) {\Huge Cycle Analysis};





%--------------------------------------------------
% REFERENCES



\end{tikzpicture}
\end{frame}



\end{document}
