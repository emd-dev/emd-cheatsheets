import emd
import numpy as np
import matplotlib.pyplot as plt

#%% ----------------------------------------------------
# Simulate some data
peak_freq = 12
sample_rate = 512
seconds = 60
x = emd.utils.ar_simulate(peak_freq, sample_rate, seconds,
                          noise_std=0.2, random_seed=42,
                          r=.99)
t = np.linspace(0, seconds, seconds*sample_rate)

# Time-series figure
plt.figure(figsize=(14, 2.5))
plt.subplots_adjust(bottom=0.3)
plt.plot(t[:sample_rate*4], x[:sample_rate*4], 'k')
plt.xlabel('Time (seconds)')
for tag in ['top','right']:
    plt.gca().spines[tag].set_visible(False)
plt.savefig('emd_software_fig1.png', dpi=300, transparent=True)

#%% ----------------------------------------------------
# Sift & IMF figure
imf = emd.sift.mask_sift(x, mask_amp_mode='ratio_sig', mask_step_factor=3)
print(imf.shape)

fig = plt.figure(figsize=(12, 6))
emd.plotting.plot_imfs(imf[:sample_rate*4,:5], cmap=True, scale_y=True, fig=fig, time_vect=t[:sample_rate*4])
plt.savefig('emd_software_fig2.png', dpi=300, transparent=True)

# Instantaneous frequency figures
IP, IF, IA = emd.spectra.frequency_transform(imf, sample_rate, 'hilbert')

fig = plt.figure(figsize=(12, 6))
plt.subplots_adjust(hspace=0.35)
plt.subplot(311)
plt.plot(t[:sample_rate*4],imf[:sample_rate*4,2],'k')
plt.plot(t[:sample_rate*4],IA[:sample_rate*4,2],'r')
plt.title('Instantaneous Amplitude (red)')
for tag in ['top','right']:
    plt.gca().spines[tag].set_visible(False)
plt.gca().set_xticklabels([])

plt.subplot(312)
plt.plot(t[:sample_rate*4], IP[:sample_rate*4,2],'b')
plt.title('Instantaneous Phase (rads)')
for tag in ['top','right']:
    plt.gca().spines[tag].set_visible(False)
plt.gca().set_xticklabels([])

plt.subplot(313)
plt.plot(t[:sample_rate*4], IF[:sample_rate*4,2],'green')
plt.title('Instantaneous Frequency (Hz)')
plt.xlabel('Time (seconds)')
plt.ylim(0,20)
for tag in ['top','right']:
    plt.gca().spines[tag].set_visible(False)
plt.savefig('emd_software_fig3.png', dpi=300, transparent=True)

#%% ----------------------------------------------------
# Spectra figures
freq_edges, freq_centres = emd.spectra.define_hist_bins(1, 50, 49)
spec = emd.spectra.hilberthuang_1d(IF, IA, freq_edges)

plt.figure(figsize=(12, 4))
plt.plot(freq_centres, spec)
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power')
for tag in ['top','right']:
    plt.gca().spines[tag].set_visible(False)
plt.legend(['IMF-1','IMF-2','IMF-3','IMF-4','IMF-5'])
plt.savefig('emd_software_fig4.png', dpi=300, transparent=True)


hht = emd.spectra.hilberthuang(IF, IA, freq_edges)

fig = plt.figure(figsize=(12, 6))
ax = plt.axes([0.125, 0.65, 0.73, 0.3])
plt.plot(t[:sample_rate*4], x[:sample_rate*4], 'k')
plt.xlim(0,4)
plt.gca().set_xticklabels([])
for tag in ['top','right']:
    plt.gca().spines[tag].set_visible(False)
ax = plt.subplot(212)
emd.plotting.plot_hilberthuang(hht[:,:sample_rate*4]**0.5, t[:sample_rate*4], freq_centres, ax=ax)
plt.savefig('emd_software_fig5.png', dpi=300, transparent=True)

#%% ----------------------------------------------------
# Cycle analysis figures
C = emd.cycles.Cycles(IP[:,2])
C.compute_cycle_metric('max_amp', IA[:,2], np.max)
C.compute_cycle_metric('duration', IA[:,2], len)
C.compute_cycle_metric('avg_if', IF[:,2], np.mean)
def degree_nonlinearity(x):
    y = np.sum(((x-x.mean()) / x.mean())**2)
    return np.sqrt(y / len(x))

C.compute_cycle_metric('DoN', IF[:, 2], degree_nonlinearity)

C.pick_cycle_subset(['is_good==1', 'duration>30', 'max_amp>12000'])

df = C.get_metric_dataframe(subset=True)

plt.figure(figsize=(14, 4))
plt.subplot(121)
plt.plot(df['avg_if'], df['max_amp'],'.')
plt.xlabel('Average IF')
plt.ylabel('Max Amplitude')
for tag in ['top','right']:
    plt.gca().spines[tag].set_visible(False)

plt.subplot(122)
plt.plot(df['duration'], df['DoN'],'.')
plt.xlabel('Duration')
plt.ylabel('Degree of Nonlinearity')
plt.ylim(0,0.25)
plt.xlim(0,70)
for tag in ['top','right']:
    plt.gca().spines[tag].set_visible(False)
plt.savefig('emd_software_fig6.png', dpi=300, transparent=True)


cycles_to_plot = [5, 23, 42, 80, 120]
plt.figure(figsize=(14, 4))
for ii in range(len(cycles_to_plot)):
    inds = C.get_inds_of_cycle(cycles_to_plot[ii])
    xinds = np.arange(len(inds)) + 55*ii
    plt.plot(xinds, x[inds], color=[0.8, 0.8, 0.8])
    plt.plot(xinds, imf[inds, 2])
for tag in ['top','right','left','bottom']:
    plt.gca().spines[tag].set_visible(False)
plt.yticks([])
plt.xticks([])

plt.savefig('emd_software_fig7.png', dpi=300, transparent=True)
