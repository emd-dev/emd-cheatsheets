# EMD Cheatsheets

Quick reference guides and summaries for the python EMD toolbox.


## Requirements

Latex environment with minted installed
Python environment with emd installed
